using Microsoft.EntityFrameworkCore;

namespace progra_web2_workshop3.Models
{
    public class DbMvcContext : DbContext
    {
        public DbMvcContext (DbContextOptions<DbMvcContext> options)
            : base(options)
        {
        }

        public DbSet<Makes> Makes { get; set; }
        public DbSet<Cars> Cars { get; set; }
    }
}