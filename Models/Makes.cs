using System;

namespace progra_web2_workshop3.Models
{
    public class Makes
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}