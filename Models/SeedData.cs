using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace progra_web2_workshop3.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new DbMvcContext(
                serviceProvider.GetRequiredService<DbContextOptions<DbMvcContext>>()))
            {
                // Look for any movies.
                if (context.Makes.Any())
                {
                    return;   // DB has been seeded
                }

                context.Makes.AddRange(
                     new Makes
                     {
                         Name = "Toyota",
                     },

                     new Makes
                     {
                         Name = "Hyundai",
                     },

                     new Makes
                     {
                         Name = "Mitsubichi",
                     },

                   new Makes
                   {
                       Name = "Subaru",
                   }
                );
                context.SaveChanges();
            }
        }
    }
}