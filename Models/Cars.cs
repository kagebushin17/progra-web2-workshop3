using System;
using System.ComponentModel.DataAnnotations;

namespace progra_web2_workshop3.Models
{
    public class Cars
    {
        public int Id { get; set; }

        public string Model { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Year { get; set; }
        public int Cylindres { get; set; }
        public int MakeId { get; set; }
        public virtual Makes Make { get; set; }
    }
}